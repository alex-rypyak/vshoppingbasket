//
//  Request.swift
//  Shopping Basket
//
//  Created by Alex R. on 02/11/2018.
//  Copyright © 2018 Alex R. All rights reserved.
//

import Foundation

protocol Endpoint {
    var base: String { get }
    var path: String { get }
    var queries: [URLQueryItem] { get }
}
extension Endpoint {
    
    var urlComponents: URLComponents? {
        
        guard var components = URLComponents(string: base)  else { return nil }
        components.path = path
        components.queryItems = queries
        return components
    }
    
    var request: URLRequest? {
        guard let url = urlComponents?.url else { return nil }
        return URLRequest(url: url)
    }
}


enum ExchangeRateRequest {
    case convert(from: String, to: String, amount:Double)
}

extension ExchangeRateRequest: Endpoint {
    
    var base: String {
        return "http://data.fixer.io"
    }
    
    var path: String {
        switch self {
        case .convert(_, _, _):
            return "/api/convert"
        }
    }
        
    var queries: [URLQueryItem] {
        
        var params = [URLQueryItem(name: "access_key", value: "ec8b9d03ebaadf32bd18266cc7067d61")]
        
        switch self {
        case .convert(let from, let to, let amount):
            
            params += [URLQueryItem(name: "from", value: from)]
            params += [URLQueryItem(name: "to", value: to)]
            params += [URLQueryItem(name: "amount", value: String(amount))]

        }
        
        return params
    }
    
}

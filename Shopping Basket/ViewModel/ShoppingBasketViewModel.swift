//
//  ShoppingBasketViewModel.swift
//  Shopping Basket
//
//  Created by Alex R. on 05/11/2018.
//  Copyright © 2018 Alex R. All rights reserved.
//

import Foundation

enum ShoppingItem: String {
    case coffee, tea, sugar, milk, cup
    
    var cost: Double {
        switch self {
        case .coffee: return 230
        case .tea: return 310
        case .sugar: return 200
        case .milk: return 120
        case .cup: return 50
        }
    }
}

class ShoppingBasketViewModel {
    
    private var basket: [ShoppingItem: Int]
    
    init() {
        basket = Dictionary<ShoppingItem, Int>()
    }
    
    var totalItems: Int {
        return basket.reduce(0, { (result, arg1) -> Int in
            let (_, value) = arg1
            return result + value
        })
    }
    
    var possibleItem: [ShoppingItem] {
        return [ShoppingItem.coffee, ShoppingItem.tea, ShoppingItem.sugar, ShoppingItem.milk, ShoppingItem.cup]
    }
    
    func add(_ item: ShoppingItem) {
        
        var value = basket[item] ?? 0
        value += 1
        basket[item] = value
        
    }
    
    func remove(_ item: ShoppingItem) {
        
        guard var value = basket[item], value > 0 else { return  }
        value -= 1
        basket[item] = value
    }
    
    func count(of item: ShoppingItem) -> Int {
        return basket[item] ?? 0
    }
    
    var totalCost: Double {
        
        var price = 0.0
        
        for (key, value) in basket {
            price += Double(value) * key.cost
        }
        
        return price
        
    }

}

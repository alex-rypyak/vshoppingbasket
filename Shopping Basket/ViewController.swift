//
//  ViewController.swift
//  Shopping Basket
//
//  Created by Alex R. on 02/11/2018.
//  Copyright © 2018 Alex R. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var viewModel: ShoppingBasketViewModel!
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // TODO: Change this to use DI
        viewModel = ShoppingBasketViewModel()
        tableView.register(ShoppingItemTableViewCell.self)
    }

}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.possibleItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeue(reusableCell: ShoppingItemTableViewCell.self, for: indexPath)
        let item = viewModel.possibleItem[indexPath.row]
        
        cell.configure(using: item, count: viewModel.count(of: item))
        cell.delegate = self
        
        return cell
        
    }
    
}

extension ViewController: ShoppingItemTableViewCellDelegate {
    
    func didIncreaseCountIn(_ cell: UITableViewCell, with item: ShoppingItem) {
        viewModel.add(item)
    }
    
    func didDecreaseCountIn(_ cell: UITableViewCell, with item: ShoppingItem) {
        viewModel.remove(item)
    }
    
}

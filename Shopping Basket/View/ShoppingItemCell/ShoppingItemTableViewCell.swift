//
//  ShoppingItemTableViewCell.swift
//  Shopping Basket
//
//  Created by Alex R. on 05/11/2018.
//  Copyright © 2018 Alex R. All rights reserved.
//

import UIKit

protocol ShoppingItemTableViewCellDelegate: class {
    func didIncreaseCountIn(_ cell: UITableViewCell, with item: ShoppingItem)
    func didDecreaseCountIn(_ cell: UITableViewCell, with item: ShoppingItem)
}

class ShoppingItemTableViewCell: UITableViewCell {

    /// The delegate
    weak var delegate: ShoppingItemTableViewCellDelegate?
    
    // What type of item to display
    private var item: ShoppingItem!
    
    // An internal reference to count
    private var currentItemCount = 0.0
    
    @IBOutlet var itemLabel: UILabel!
    
    func configure(using item: ShoppingItem, count: Int) {
        
        itemLabel.text = item.rawValue.capitalized
        currentItemCount = Double(count)
    }
    
    @IBAction func didUpdateStepper(_ sender: UIStepper) {
        
        sender.value > currentItemCount ? delegate?.didIncreaseCountIn(self, with: item) : delegate?.didDecreaseCountIn(self, with: item)
        currentItemCount = sender.value
        
    }
    
}

extension ShoppingItemTableViewCell: Dequeuable {}

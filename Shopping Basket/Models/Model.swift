//
//  Model.swift
//  Shopping Basket
//
//  Created by Alex R. on 02/11/2018.
//  Copyright © 2018 Alex R. All rights reserved.
//

import Foundation

struct Query: Codable {
    
    /// The currency converted to.
    var to: String
    
    /// The currency converted from.
    var from: String
    
    ///  The amount that is converted.
    var amount: Double
}

struct ConvertResponse: Codable {
    
    /// true or false depending on whether or not the API request has succeeded.
    var success: Bool
    
    /// Information about the query submitted
    var query: Query
    
    /// The conversion result.
    var result: Double
}


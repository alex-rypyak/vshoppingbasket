//
//  DataParserTests.swift
//  Shopping BasketTests
//
//  Created by Alex R. on 02/11/2018.
//  Copyright © 2018 Alex R. All rights reserved.
//

import XCTest
@testable import Shopping_Basket

class DataParserTests: XCTestCase {
    
    var testBundle: Bundle!
    
    override func setUp() {
        testBundle = Bundle(for: type(of: self))
    }
    
    override func tearDown() {
        testBundle = nil
    }
    
    func testEmptyData_ParserThrowsException() {
        
        // Given empty data
        let emptyData = Data()
        
        // When we parse, an exception should be thrown
        XCTAssertThrowsError(try DataParser.parse(emptyData, type: ConvertResponse.self))
        
    }
    
    func testInavlidData_ParserThrowsException() {
        
        let invalidData = "Hello, world".data(using: .utf8)!
        
        XCTAssertThrowsError(try DataParser.parse(invalidData, type: ConvertResponse.self))
    }
    
    func testSampleData_ParserNoThrow() {
        
        let sampleDataPath = testBundle.path(forResource: "SampleConvertResponse", ofType: "json")!
        let simpleData = try! Data(contentsOf: URL(fileURLWithPath: sampleDataPath))
        
        XCTAssertNoThrow(try DataParser.parse(simpleData, type: ConvertResponse.self))
    }
    
    func testSampleData_ParsedHasResult() {
        
        let sampleDataPath = testBundle.path(forResource: "SampleConvertResponse", ofType: "json")!
        let simpleData = try! Data(contentsOf: URL(fileURLWithPath: sampleDataPath))
        
        do {
            let result = try DataParser.parse(simpleData, type: ConvertResponse.self)
            let count = result.result
            
            XCTAssertGreaterThanOrEqual(count, 0.0)
        } catch {
            XCTFail("\(error)")
        }
        
    }
    
}

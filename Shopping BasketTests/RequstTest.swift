//
//  RequstTest.swift
//  Shopping BasketTests
//
//  Created by Alex R. on 02/11/2018.
//  Copyright © 2018 Alex R. All rights reserved.
//

import XCTest
@testable import Shopping_Basket

class RequstTest: XCTestCase {
    
    private let expectedURLComps = URLComponents(string: "http://data.fixer.io/api/convert?access_key=ec8b9d03ebaadf32bd18266cc7067d61&from=GBP&to=JPY&amount=25")
    
    var systemUnderTest: ExchangeRateRequest!

    override func setUp() {
        systemUnderTest = ExchangeRateRequest.convert(from: "GBP", to: "JPY", amount: 25.0)
    }

    override func tearDown() {
        systemUnderTest = nil
    }

    func testConvertRequest_NonNilURLComponents() {
        
        // When we get the URLComps
        let result = systemUnderTest.urlComponents
        
        // Then the URLComps should NOT be nil
        XCTAssertNotNil(result)
        
    }
    
    func testConvertRequest_NonNilURL() {
        
        // When we get the URL form the URLComps
        let result = systemUnderTest.urlComponents?.url
        
        // Then the URL should NOT be nil
        XCTAssertNotNil(result)
        
    }
    
    func testConvertRequest_ValidHost() {
        
        // When we get Post
        let sutHost = systemUnderTest.urlComponents?.host
        let expectedHost = expectedURLComps!.host!
        
        // Then when we comapre them
        let result = sutHost?.compare(expectedHost)
        
        // They should be the same
        XCTAssertEqual(result, ComparisonResult.orderedSame, "Invalid API Host")
        
    }
    
    func testConvertRequest_ValidPath() {
        
        // When we get the Path
        let sutPath = systemUnderTest.urlComponents?.path
        let expectedPath = expectedURLComps!.path
        
        // Then when we compare them
        let result = sutPath?.compare(expectedPath)
        
        // They should be the same
        XCTAssertEqual(result, ComparisonResult.orderedSame, "Invalid API Path")
        
    }

}

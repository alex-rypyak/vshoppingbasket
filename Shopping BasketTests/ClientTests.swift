//
//  ClientTests.swift
//  Shopping BasketTests
//
//  Created by Alex R. on 02/11/2018.
//  Copyright © 2018 Alex R. All rights reserved.
//

import XCTest
@testable import Shopping_Basket

class ClientTests: XCTestCase {

    private let defaultTimeout = 15.0
    var systemUnderTest: FixerClient!
    
    override func setUp() {
        systemUnderTest = FixerClient(configuration: .ephemeral)
    }
    
    override func tearDown() {
        systemUnderTest = nil
    }

    func testValidAPIConnection() {
        
        let request = ExchangeRateRequest.convert(from: "GBP", to: "EUR", amount: 10.0)
        let promise = expectation(description: "Valid API Connection")
        
        systemUnderTest.getConversion(from: request) { _ in
            promise.fulfill()
        }
        
        waitForExpectations(timeout: defaultTimeout, handler: nil)
    }
    
    func testReceivedDataFromServer() {
        
        let request = ExchangeRateRequest.convert(from: "GBP", to: "EUR", amount: 10.0)
        let promise = expectation(description: "Received Data from API")
        var connectionResult: Result<Data, APIError>?
        
        systemUnderTest.getConversion(from: request) { result in
            connectionResult = result
            promise.fulfill()
        }
        
        waitForExpectations(timeout: defaultTimeout, handler: nil)
        
        guard let actualResult = connectionResult else { return XCTFail() }
        
        switch actualResult {
        case .success(_): break
        case .failure(let error): XCTFail(error.localizedDescription)
        }
    }

}

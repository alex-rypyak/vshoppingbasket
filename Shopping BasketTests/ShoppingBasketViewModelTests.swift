//
//  ShoppingBasketViewModelTests.swift
//  Shopping BasketTests
//
//  Created by Alex R. on 05/11/2018.
//  Copyright © 2018 Alex R. All rights reserved.
//

import XCTest
@testable import Shopping_Basket

class ShoppingBasketViewModelTests: XCTestCase {
    
    var sut: ShoppingBasketViewModel!

    override func setUp() {
        sut = ShoppingBasketViewModel()
    }

    override func tearDown() {
        sut = nil
    }
    
    func testNewShoppingBasketModel_ModelIsEmpty() {
        
        // Given we create a new model, as per setup
        
        // When we check the number of item, it should be 0
        let result = sut.totalItems
        
        // Then the number of items should be 0
        XCTAssertEqual(0, result)
        
    }
    
    func testEmptyModel_AddAnItem_ItemCountisOne() {
        
        // Given we create a new model, as per setup
        
        // When add an item
        sut.add(.coffee)
        
        // Then the number of items should be 1
        let result = sut.totalItems
        XCTAssertEqual(1, result)
        
    }
    
    func testEmptyModel_AddThreeItems_ItemCountisThree() {

        // When add 3 items
        sut.add(.coffee)
        sut.add(.milk)
        sut.add(.cup)
        
        // Then the number of items should be 3
        let result = sut.totalItems
        
        XCTAssertEqual(3, result)
        
    }
    
    func testEmptyModel_RemoveAnItem_ItemCountIsZero() {
        
        // When we remove an item
        sut.remove(.coffee)
        let result = sut.totalItems
        
        // Then the number of items should be 0
        XCTAssertEqual(0, result)
        
    }
    
    func testEmptyModel_RemoveMultipleItems_ItemCountIsZero() {
        
        // When we remove an item
        sut.remove(.coffee)
        sut.remove(.cup)
        sut.remove(.sugar)
        
        let result = sut.totalItems
        
        // Then the number of items should be 0
        XCTAssertEqual(0, result)
        
    }
    
    func testEmptyModel_TotalCostOfItemIsZero() {
        
        // When we get the total cost of the basket
        let result = sut.totalCost
        
        // Then the cost should be 0
        XCTAssertEqual(0.0, result)
        
    }
    
    func testModel_ContainsThreeCoffee_TotalCostIsCorrect() {
        
        // When we remove add 3 Coffee Items
        sut.add(.coffee)
        sut.add(.coffee)
        sut.add(.coffee)
        
        // Then the number of cost of the basket should be 690
        let result = sut.totalCost
        let expectCost = ShoppingItem.coffee.cost * 3
        XCTAssertEqual(expectCost, result)
        
    }
    
    func testModel_ContainsThreeTea_TotalCostIsCorrect() {
        
        // When we remove add 3 Tea Items
        sut.add(.tea)
        sut.add(.tea)
        sut.add(.tea)
        
        // Then the number of cost of the basket should be 690
        let result = sut.totalCost
        let expectCost = ShoppingItem.tea.cost * 3
        XCTAssertEqual(expectCost, result)
        
    }
    
    func testModel_ContainsThreeSugar_TotalCostIsCorrect() {
        
        // When we remove add 3 Sugar Items
        sut.add(.sugar)
        sut.add(.sugar)
        sut.add(.sugar)
        
        // Then the number of cost of the basket should be 690
        let result = sut.totalCost
        let expectCost = ShoppingItem.sugar.cost * 3
        XCTAssertEqual(expectCost, result)
        
    }
    
    func testModel_ContainsThreeMilk_TotalCostIsCorrect() {
        
        // When we remove add 3 Milk Items
        sut.add(.coffee)
        sut.add(.coffee)
        sut.add(.coffee)
        
        // Then the number of cost of the basket should be 690
        let result = sut.totalCost
        let expectCost = ShoppingItem.coffee.cost * 3
        XCTAssertEqual(expectCost, result)
        
    }
    
    func testModel_ContainsThreeCup_TotalCostIsCorrect() {
        
        // When we remove add 3 Cup Items
        sut.add(.cup)
        sut.add(.cup)
        sut.add(.cup)
        
        // Then the number of cost of the basket should be 690
        let result = sut.totalCost
        let expectCost = ShoppingItem.cup.cost * 3
        XCTAssertEqual(expectCost, result)
        
    }
    
    func testModel_ContainsOfEachItem_TotalCostIsCorrect() {
        
        sut.add(.coffee)
        sut.add(.cup)
        sut.add(.milk)
        sut.add(.sugar)
        sut.add(.tea)
        
        let result = sut.totalCost
        
        let expectedCost = ShoppingItem.coffee.cost + ShoppingItem.cup.cost + ShoppingItem.milk.cost + ShoppingItem.sugar.cost + ShoppingItem.tea.cost
        
        XCTAssertEqual(expectedCost, result)
        
    }

}
